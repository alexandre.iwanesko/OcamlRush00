#!/usr/bin/env bash

binary="$1"

function start {
	echo "tana"
}
function place {
	local x y
	x="$1"
	y="$2"

	echo "$x $y"
}

function magic {
	local func
	func="$1"
	$func 0 0 # pl1
	$func 3 0 # pl2
	$func 1 0 # pl1
	$func 4 0 # pl2
	$func 2 0 # pl1
	$func 5 0 # pl2

	$func 0 3 # pl1
	$func 3 3 # pl2
	$func 1 3 # pl1
	$func 4 3 # pl2
	$func 2 3 # pl1
	$func 5 3 # pl2

	$func 0 6 # pl1
	$func 3 6 # pl2
	$func 1 6 # pl1
	$func 4 6 # pl2
	$func 2 6 # pl1
	$func 5 6 # pl2

}

ocamlrun -b $binary < <(start; magic place)
