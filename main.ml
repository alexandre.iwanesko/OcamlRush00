type player = { name: string; ch: char; lcorn: char; rcorn: char; uside: char; bside: char }
type board = { bd: string; level1: board; dimx: int; dimy: int; xbreak: int; ybreak: int }
type 'a maybe = Nothing | Just of 'a
type color = EOC | BLACK | RED | GREEN | ORANGE | BLUE | PURPLE | CYAN | LGRAY | DGRAY | LRED | LGREEN | YELLOW | LBLUE | LPURPLE | LCYAN | WHITE

let get_color = function
  | EOC -> "\x1b[0m"
  | BLACK -> "\x1b[0;30m"
  | RED -> "\x1b[0;31m"
  | GREEN -> "\x1b[0;32m"
  | ORANGE -> "\x1b[0;33m"
  | BLUE -> "\x1b[0;34m"
  | PURPLE -> "\x1b[0;35m"
  | CYAN -> "\x1b[0;36m"
  | LGRAY -> "\x1b[0;37m"
  | DGRAY -> "\x1b[1;30m"
  | LRED -> "\x1b[1;31m"
  | LGREEN -> "\x1b[1;32m"
  | YELLOW -> "\x1b[1;33m"
  | LBLUE -> "\x1b[1;34m"
  | LPURPLE -> "\x1b[1;35m"
  | LCYAN -> "\x1b[1;36m"
  | WHITE -> "\x1b[1;37m"

let prompt msg =
        print_string msg;
        read_line ()

let set = String.set

let print_ints lst =
        let rec magic lst = match lst with
                  [] -> print_endline ""
                | x :: xs -> print_int x; print_string " "; magic xs in
        magic lst

let atoi str =
        let len = String.length str in
        let is_num c = c >= '0' && c <= '9' in
        let rec _atoi i acc =
                if i = len then Just acc
                else if not @@ is_num str.[i] then Nothing
                else _atoi (i + 1) (acc * 10 + (int_of_char str.[i] - int_of_char '0')) in
        _atoi 0 0

let split str =
        let rstr = String.trim str in
        let len = String.length rstr in
        if String.contains rstr ' ' then begin
                let index = String.index rstr ' ' in
                let first = String.trim @@ String.sub rstr 0 index in
                let sec   = String.trim @@ String.sub rstr index (len - index) in
                [first; sec;]
        end
                else []

(* Alignment for all al not powers of 2 *)
let align_down x al = (x / al) * al
let align_up x al = x + al - (x mod al)

let get_level1_square board x' y' = (x' / board.level1.xbreak + (y' / board.level1.ybreak) * board.level1.dimx)

let game plr1 plr2 dimx dimy =

        let rec get_pl_move plr =
                let resp = split @@ prompt (plr.name ^ ", enter move: ") in
                if List.length resp <> 2 then begin print_endline "Bad format"; get_pl_move plr end
                else match (atoi @@ List.hd resp, atoi @@ List.hd @@ List.tl resp) with
                          (Just x, Just y) when  x < dimx && y < dimy -> (x, y)
                        | _                -> print_endline "Illegal coordinate"; get_pl_move plr in


        let print_board board =
                let rec print_row x y =
                        if x = board.dimx then begin print_endline ""; print_row 0 (y + 1) end
                        else if y >= board.dimy then ()
                        else begin
                               if y > 0 && x = 0 && y mod board.ybreak = 0 then print_endline @@ String.make (board.dimx * 2) '-' else ();
                               if x > 0 && x mod board.xbreak = 0 then print_string "|" else ();
                               print_char @@ String.get board.bd (x + y * board.dimx);
                               print_char ' ';
                               print_row (x + 1) y;
                        end in
                print_row 0 0 in

        let claim_partition x y board plr =
                let x' = align_down x board.xbreak in
                let xe = align_up x board.xbreak in
                let y' = align_down y board.ybreak in
                let ye = align_up y board.ybreak in
                let place_angles () =
                        set board.bd (x' + y' * board.dimx) plr.lcorn;
                        set board.bd (xe - 1 + y' * board.dimx) plr.rcorn;
                        set board.bd (x' + (ye - 1) * board.dimx) plr.rcorn;
                        set board.bd (xe - 1 + (ye - 1) * board.dimx) plr.lcorn in
                let place_line b m x y =
                        let rec magic x' =
                                if x' >= xe then () else begin
                                set board.bd (x' + y * board.dimx) @@ if x' = x || x' = board.dimx - 1 then  b
                                                                                else m;
                                magic (x' + 1) end in
                        magic x in
                let rec place_lines i =
                        if i >= ye then ()
                        else begin
                                place_line plr.bside plr.uside x' i;
                                place_lines (i + 1)
                        end in
                place_lines y';
                place_angles ();
                (* print_endline "IN CLAIM"; *)
                (* print_ints [x' + 1; y' + 1; board.dimx]; *)
                set board.bd (x' + 1 + (y' + 1) * board.dimx) plr.ch;
                set board.level1.bd (get_level1_square board x' y') plr.ch;
                print_endline board.level1.bd in

        let add_tick board x y plr =
                let check_horiz x y board =
                        let x' = align_down x board.xbreak in

                        let rec check i = (*print_ints [i; x'; board.xbreak;board.dimx; y;] ;*)
                                        if i - x' >= board.xbreak then true
                                          else if String.get board.bd (i + board.dimx * y) = plr.ch
                                               then check (i + 1)
                                          else begin print_char @@ String.get board.bd (i + board.dimx * y); print_endline ""; false end in
                        check x' in
                let check_vert x y board =
                        let y' = align_down y board.ybreak in
                        let rec check i = if i - y' >= board.ybreak then true
                                          else if String.get board.bd (x + board.dimx * i) = plr.ch
                                               then check (i + 1)
                                          else false in
                        check y' in
                let check_diag_left_up x y board =
                        let x' = align_up x board.xbreak - 1 in
                        let y' = align_down y board.ybreak in
                        let rec check x y = if y - y' >= board.ybreak then true
                                          else if String.get board.bd (x + board.dimx * y) = plr.ch
                                               then check (x - 1) (y + 1)
                                          else false in
                        check x' y' in
                let check_diag_right_up x y board =
                        let x' = align_down x board.xbreak in
                        let y' = align_down y board.ybreak in
                        let rec check x y = if y - y' >= board.ybreak then true
                                          else if String.get board.bd (x + board.dimx * y) = plr.ch
                                               then check (x + 1) (y + 1)
                                          else false in
                        check x' y' in
                let check_full x y board =
                        let x' = align_down x board.xbreak in
                        let y' = align_down y board.ybreak in
                        let ye = align_up y board.ybreak in
                        let xe = align_up x board.ybreak in
                        print_ints [x';y';ye;xe;];
                        let rec check x y = if x >= xe then check x' (y + 1)
                                            else if y >= ye then true
                                            else if String.get board.bd (x + y * board.dimx) = '-'
                                            then false else check (x + 1) y in
                        check x' y' in

                let check_all x y board
                        = check_horiz x y board || check_vert x y board
                          || check_diag_left_up x y board || check_diag_right_up x y board || check_full x y board in

                print_int x;
                print_string "   ";
                print_int y;
                print_string "   ";
                print_int board.dimx;
                print_endline "";
                set board.bd (x + board.dimx * y) plr.ch;
                if check_all x y board then begin
                        claim_partition x y board plr;
                        let square = get_level1_square board x y in
                        let (x', y') = (square mod board.level1.xbreak, square / board.level1.ybreak) in
                        check_all x' y' board.level1
                        end
                        else false
                in

        let announce_winner plr =
                print_endline @@ plr.name ^ " won !" in
        let announce_draw () =
                print_endline "Game draw !" in

        let rec play board plr1 plr2=
                let (x, y) = get_pl_move plr1 in
                if String.get board.level1.bd (get_level1_square board x y) <> '-' ||  String.get board.bd (x + y * board.dimx)  <> '-'
                then begin print_endline "Position already taken"; play board plr1 plr2 end
                else
                        begin
                                let win = add_tick board x y plr1 in
                                print_board board;
                                if win then announce_winner plr1
                                else if String.contains board.level1.bd '-'
                                        then play board plr2 plr1
                                else announce_draw ()
                        end
                in

        let ybreak = int_of_float @@ sqrt @@ float_of_int dimy in
        let xbreak = int_of_float @@ sqrt @@ float_of_int dimx in

        let rec board = { bd = String.make (dimx * dimy) '-';
                        dimx = dimx;
                        dimy = dimy;
                        xbreak = xbreak;
                        ybreak = ybreak;
                      level1 = _end_ }
           and _end_ = {bd = String.make dimx '-';
                        dimx = xbreak;
                        dimy = ybreak;
                        xbreak = xbreak;
                        ybreak = ybreak;
                        level1 = board;
                      } in
        print_board board;
        play board plr1 plr2

let ascii_art () =
print_endline ((get_color CYAN) ^ "  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄       ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄       ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ " ^ (get_color EOC));
print_endline ((get_color CYAN) ^ " ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌" ^ (get_color EOC));
print_endline ((get_color CYAN) ^ "  ▀▀▀▀█░█▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀       ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀       ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ " ^ (get_color EOC));
print_endline ((get_color CYAN) ^ "      ▐░▌          ▐░▌     ▐░▌                    ▐░▌     ▐░▌       ▐░▌▐░▌                    ▐░▌     ▐░▌       ▐░▌▐░▌          " ^ (get_color EOC));
print_endline ((get_color CYAN) ^ "      ▐░▌          ▐░▌     ▐░▌                    ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░▌                    ▐░▌     ▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄ " ^ (get_color EOC));
print_endline ((get_color CYAN) ^ "      ▐░▌          ▐░▌     ▐░▌                    ▐░▌     ▐░░░░░░░░░░░▌▐░▌                    ▐░▌     ▐░▌       ▐░▌▐░░░░░░░░░░░▌" ^ (get_color EOC));
print_endline ((get_color CYAN) ^ "      ▐░▌          ▐░▌     ▐░▌                    ▐░▌     ▐░█▀▀▀▀▀▀▀█░▌▐░▌                    ▐░▌     ▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀ " ^ (get_color EOC));
print_endline ((get_color CYAN) ^ "      ▐░▌          ▐░▌     ▐░▌                    ▐░▌     ▐░▌       ▐░▌▐░▌                    ▐░▌     ▐░▌       ▐░▌▐░▌          " ^ (get_color EOC));
print_endline ((get_color CYAN) ^ "      ▐░▌      ▄▄▄▄█░█▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄           ▐░▌     ▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄           ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ " ^ (get_color EOC));
print_endline ((get_color CYAN) ^ "      ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌          ▐░▌     ▐░▌       ▐░▌▐░░░░░░░░░░░▌          ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌" ^ (get_color EOC));
print_endline ((get_color CYAN) ^ "       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀            ▀       ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀            ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ " ^ (get_color EOC))

let rec get_name n1 n2 clr = match n1 with
  | "" when n2 = ""  -> let n = prompt ((get_color clr) ^ "Player 1: " ^ (get_color EOC)) in get_name n n2 RED
  | "" when n2 <> "" -> let n = prompt ((get_color clr) ^ "Player 2: " ^ (get_color EOC)) in get_name n n2 RED
  | x when x = n2    -> let n = prompt ((get_color clr) ^ "Player 2: " ^ (get_color EOC)) in get_name n n2 RED
  | _                -> n1

let () =
        ascii_art ();
        let dim = 9 in
        let name1 = print_endline "Enter your name"; get_name "" "" LGREEN in
        let name2 = get_name "" name1 LGREEN in
        let plr1 = {name = name1; ch = '0'; lcorn = '/'; rcorn = '\\'; uside = ' '; bside = ' '} in
        let plr2 = {name = name2; ch = 'X'; lcorn = '\\'; rcorn = '/'; uside = ' '; bside = ' '} in
        game plr1 plr2 dim dim
