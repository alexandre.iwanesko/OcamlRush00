#!/usr/bin/env bash

square="$(test -z "$1" && echo "0" || echo "$1")"
binary="$2"

if [ -z "$binary" ]; then
	>&2 echo "Which binary ?!"
	exit 1
fi

if [ "$square" -ge 9 ]; then
	>&2 echo "Maximum square number : 9 - $square given"
	exit 1
fi

start_line="$(expr $(expr $square / 3) \* 3)"
end_line=$(expr $start_line + 3)

function start {
	echo "Tana"
}

function horizontal {
	local x y
	x="$1"
	y="$2"

	echo -e "$x $y\n$(expr $x + 1) $y\n$(expr $x + 2) $y\n"
}

function vertical {
	local x y
	x="$1"
	y="$2"

	echo -e "$x $y\n$x $(expr $y + 1)\n$x $(expr $y + 2)\n"
}

function left_diag {
	local x y
	x="$1"
	y="$2"

	echo -e "$x $(expr $y + 2)\n$(expr $x + 1) $(expr $y + 1)\n$(expr $x + 2) $y\n"
}

function right_diag {
	local x y
	x="$1"
	y="$2"

	echo -e "$(expr $x + 2) $(expr $y + 2)\n$(expr $x + 1) $(expr $y + 1)\n$x $y\n"
}

function run_y {
	local msg i x func loop
	msg="$1"
	func="$2"
	x=0
	i=$start_line

	echo "Running $msg tests"

	while [ "$i" -lt "$end_line" ]; do
		echo "Line $i"
		$binary < <(start; $func 0 $i)
		i=$((i + 1))
		PROMPT="Press enter to continue" read
	done
}

function run_x {
	local msg i x func loop
	msg="$1"
	func="$2"
	x=0
	i=$start_line

	echo "Running $msg tests"

	while [ "$i" -lt "$end_line" ]; do
		echo "Line $i"
		$binary < <(start; $func $x $start_line)
		i=$((i + 1))
		x=$((x + 1))
		PROMPT="Press enter to continue" read
	done
}

run_x "horizontal" horizontal
run_y "vertical" vertical

echo "Running left diag tests"
$binary < <(start; left_diag 0 $start_line)

echo "Running right diag tests"

$binary < <(start; right_diag 0 $start_line)
